/*Теоретичні питання та відповіді.
    1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
    У JavaScript, коли об'єкт потребує доступу до якоїсь властивості чи методу, він спочатку перевіряє свої власні властивості. 
    Якщо він не знаходить там потрібного, то перевіряє властивості свого прототипу, і так далі, вглиб ієрархії прототипів.
   
    2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
    super() викликає конструктор класу-батька, і це потрібно, щоб успадкувати властивості та функціональність класу-батька у клас-нащадок. 
    Таким чином, ми можемо використовувати код з класу-батька в класі-нащадку.


/*Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Література:
Класи на MDN
Класи в ECMAScript 6*/


// Створюємо клас Employee із властивостями, гетерами та сетерами
class Employee {
  constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
  }

  get name() {
      return this._name;
  }

  set name(value) {
      this._name = value;
  }

  get age() {
      return this._age;
  }

  set age(value) {
      this._age = value;
  }

  get salary() {
      return this._salary;
  }

  set salary(value) {
      this._salary = value;
  }
}

//Створюєио клас Programmer, який успадковує властивості від класу Employee та має властивість lang

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
  }

  set lang(value) {
      this._lang = value;
  }

  get lang() {
      return this._lang;
  }

  get salary() {
      return this._salary * 3;
  }

}

// Створюємо об'єкти класу Programmer
const programmer1 = new Programmer('Arseniy', 23, 47000, ['C#', 'Python']);
const programmer2 = new Programmer('Sasha', 37, 39000, ['Java', 'C++']);
const programmer3 = new Programmer('John', 82, 1000000, ['Speedcoding', 'Fortran']);

// Виводимо їх у консоль
console.log('Програміст 1:');
console.log('Ім\'я:', programmer1.name);
console.log('Вік:', programmer1.age);
console.log('Зарплата:', programmer1.salary);
console.log('Мови програмування:', programmer1.lang);

console.log('Програміст 2:');
console.log('Ім\'я:', programmer2.name);
console.log('Вік:', programmer2.age);
console.log('Зарплата:', programmer2.salary);
console.log('Мови програмування:', programmer2.lang);

console.log('Програміст 3:');
console.log('Ім\'я:', programmer3.name);
console.log('Вік:', programmer3.age);
console.log('Зарплата:', programmer3.salary);
console.log('Мови програмування:', programmer3.lang);


